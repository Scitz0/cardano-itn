#!/bin/bash

USAGE="\n$(basename $0) [-h] <epoch> -- program to calculate the difference in time between scheduled, wake and finished\n\nwhere:\n\t-h\tshow this help text\n\tepoch\tepoch to show\n\n"

if [ $# -eq 0 ] || [ $1 == "-h" ]; then
  printf "$USAGE"
  exit
fi

BLOCKS_JSON=/var/www/html/blocks.json

printf "\n %-5s | %-5s | %-9s | %-19s | %-11s | %-23s\n " EPOCH SLOT MINTED "SCHEDULED (UTC)" "WAKE (skd.)" "FINISHED (skd. / wake)"

for row in $(cat "$BLOCKS_JSON" | jq -r '.[] | @base64'); do
  _jq() {
    echo ${row} | base64 --decode | jq -r ${1}
  }
  EPOCH=$(_jq '.scheduled_at_date' | cut -d'.' -f 1)
  if [ $# -eq 1 ] && [ $1 -ne $EPOCH ]; then
    # Epoch specified and current block does not belong to this epoch
    continue
  fi
  SLOT=$(_jq '.scheduled_at_date' | cut -d'.' -f 2)
  MINTED=$(_jq '.minted')
  [ $MINTED == null ] && MINTED="pending"
  SCHEDULED_AT_TIME=$(_jq '.scheduled_at_time')
  SCHEDULED_AT_TIME_UNIX=$(date "+%s" -d "$SCHEDULED_AT_TIME")
  SCHEDULED_AT_TIME=$(echo "$SCHEDULED_AT_TIME=" | sed 's/\(.*\)T\([0-9]*:[0-9]*:[0-9]*\).*/\1 \2/')
  WAKE_AT_TIME=$(_jq '.wake_at_time')
  FINISHED_AT_TIME=$(_jq '.finished_at_time')
  if [ ! $WAKE_AT_TIME == null ] && [ ! $FINISHED_AT_TIME == null ]; then
    [ $MINTED == "pending" ] && MINTED="completed"
    WAKE_AT_TIME_UNIX=$(date "+%s" -d "$WAKE_AT_TIME")
    WAKE_AT_TIME_NANO=$(echo "$WAKE_AT_TIME=" | sed 's/.*\.\([0-9]*\).*/\1/')
    WAKE_AT_TIME_DIFF=$(echo "${WAKE_AT_TIME_UNIX}.${WAKE_AT_TIME_NANO} - ${SCHEDULED_AT_TIME_UNIX}" | bc)
    FINISHED_AT_TIME_UNIX=$(date "+%s" -d "$FINISHED_AT_TIME")
    FINISHED_AT_TIME_NANO=$(echo "$FINISHED_AT_TIME=" | sed 's/.*\.\([0-9]*\).*/\1/')
    FINISHED_AT_TIME_DIFF_S=$(echo "${FINISHED_AT_TIME_UNIX}.${FINISHED_AT_TIME_NANO} - ${SCHEDULED_AT_TIME_UNIX}" | bc)
    FINISHED_AT_TIME_DIFF_W=$(echo "${FINISHED_AT_TIME_UNIX}.${FINISHED_AT_TIME_NANO} - ${WAKE_AT_TIME_UNIX}.${WAKE_AT_TIME_NANO}" | bc)
  else
    [ $WAKE_AT_TIME == null ] && WAKE_AT_TIME=""
    [ $FINISHED_AT_TIME == null ] && FINISHED_AT_TIME=""
  fi
  if [[ "$(date -u +'%Y-%m-%d %T')" > "$SCHEDULED_AT_TIME" ]]; then
    [ $MINTED == pending ] && MINTED="missed"
  fi
  printf "%-5s | %-5s | %-9s | %-19s | %-11s | %-23s\n " $EPOCH $SLOT $MINTED "$SCHEDULED_AT_TIME" "$WAKE_AT_TIME_DIFF" "$FINISHED_AT_TIME_DIFF_S / $FINISHED_AT_TIME_DIFF_W"
done
printf "\n"