## Pool Saturation Analysis 

Below pool saturation analysis is based on current status of ITN as of 1/5 2020.  
This analysis is meant to help delegators interpret set saturation point and how it affects the rewards.  

**Let's start with some facts**  
There are currently 12.8 b₳ staked on the ITN.  
The saturation point, also known as "k" value, is set at 1% for the ITN (1/k=100).  
1% of 12.8 b₳ gives us a saturation point at 128 m₳.  

**So how does one interpret the above facts?**  
To start, staying below saturation point doesn’t mean that you are not affected by saturation.  
I argue that you shouldn’t delegate to a pool that has reached 80% of the delegation point if you want to avoid rewards being capped.  

**Some numbers**  
As a general rule, slot assignment per million ₳ staked ranges from 2-4.  
Edge cases with more and less exist but are rare as delegation increases.  
I will later refer to these as good(2m), normal(3m) and bad(4m).  
Due to saturation point there is a ceiling around 34-35 k₳ in total staker rewards.  
Around 800-860 ₳ is awarded / block, varies depending on how many blocks are made in total for that epoch. Based on the fact that the overall network health is good and ~4000 blocks are made that epoch. 830 ₳ will be used in examples below.  
Currently everything above 40-44 blocks is a waste based on assumption of 800-860 ₳ reward / block.  

**Math time**  
Example:  
Pool delegation is at 120m close to saturation point.  
Based on above numbers, a “normal” epoch should give pool 40 block leader slots (120/3).  
40*830 = 33.2 k₳ ... meaning you are fine, rewards are not capped and ROS is ok as you had a "normal" slot assignment based on stake.  
Then you have a good epoch with 60 slots assigned (2m/block).  
60*830 = 49.8 k₳ ... but rewards are capped at 34-35 k₳ meaning you won't do any better than the "normal" epoch.  
Then you have a bad epoch with 30 slots assigned, 4m/block.  
30*830 = 14.9 k₳ ...  ROS drops :(  

**Summary**  
When close to saturation point you will only have normal and bad epochs, the good are turned into normal due to saturation point. The good epoch’s aren't able to compensate for the bad ones as the rewards are capped. A good epoch with 2 m₳ per slot assigned puts ceiling at 80-88 m₳ or ~70% of saturation point to not risk rewards being capped. Other factors like slot battles and missed blocks due to various other reasons will have to be accounted for as well. Pool can't be expected to make all assigned blocks. To account for this and pool delegation fluctuation the ceiling should be raised with another 5-10%. This then puts us at 75-80% of saturation point or around 100 m₳.  

The extra room up to the saturation point gives the good epoch’s the ability to negate the bad ones and give an optimal mix of stability/regular block creation without risking good epoch’s to be capped.
